import 'package:flutter/material.dart';

abstract class Languages {
  static Languages of(BuildContext context) {
    return Localizations.of<Languages>(context, Languages);
  }

  String get appName;

  String get labelNextLanguageName;

  String get labelGetToken;

  String get labelShortenUrl;

  String get labelSelectLanguage;

  String get labelToken;

  String get labelTargetUrl;

  String get labelUrl;

  String get labelAbout;

  String get labelAboutUs;

  String get labelContactUs;

  String get labelHelp;

  String get labelFAQ;

  String get labelSocial;

  String get labelLinkedin;

  String get labelTrendyolBootcamp;

  String get labelKodluyoruzTrendyolBootcamp;

  String get labelGroupName;

  String get labelFinalProjectGroupName;

  String get labelCopyright2020;

  String get labelDiscover;

  String get labelPage1Header;

  String get labelPage2Header;

  String get labelPage3Header;

  String get labelPage4Header;

  String get labelUrlNotValidMessage;

  String get labelTokenNotValidMessage;

  String get labelShortenButtonText;

  String get labelAgreePrivacyPolicy;

  String get labelWelcome;

  String get labelEmailAddress;

  String get labelEmail;

  String get labelName;

  String get labelYourName;

  String get labelGetUserInfoButtonText;

  String get labelSave;

  String get apiErrorMessage;

  String get labelGetUserAllLinksButtonText;

  String get labelStatus;

  String get labelLinkStatusValid;

  String get labelLinkStatusInvalid;

  String get labelExpireDate;

  String get labelPassword;

  String get labelDescription;

  String get labelEdit;

  String get labelUsageCounter;

  String get labelCurrentlyNotAvailable;

  String get labelDeleteAccount;

  String get labelDelete;
}
