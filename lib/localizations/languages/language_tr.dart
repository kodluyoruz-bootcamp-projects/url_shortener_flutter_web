import 'languages.dart';

class LanguageTr extends Languages {
  @override
  String get appName => "URL SHORTENER";

  @override
  String get labelNextLanguageName => "English";

  @override
  String get labelGetToken => "Token Al";

  @override
  String get labelShortenUrl => "Shorten Url";

  @override
  String get labelSelectLanguage => "Select Language";

  @override
  String get labelToken => "Token";

  @override
  String get labelTargetUrl => "Hedef Link";

  @override
  String get labelUrl => "Link";

  @override
  String get labelAbout => "Hakkında";

  @override
  String get labelAboutUs => "Hakkımızda";

  @override
  String get labelContactUs => "Bize Ulaşın";

  @override
  String get labelHelp => "Yardım";

  @override
  String get labelFAQ => "SSS";

  @override
  String get labelSocial => "Sosyal Medya";

  @override
  String get labelLinkedin => "Linkedin";

  @override
  String get labelTrendyolBootcamp => "Trendyol Bootcamp ";

  @override
  String get labelKodluyoruzTrendyolBootcamp => "Kodluyoruz Trendyol Bootcamp ";

  @override
  String get labelGroupName => "Grup 2";

  @override
  String get labelFinalProjectGroupName => "Bitirme Projesi Grup 2";

  @override
  String get labelCopyright2020 => "Telif hakkı © 2020";

  @override
  String get labelDiscover => "Keşfet";

  @override
  String get labelPage1Header => "Link Kısalt";

  @override
  String get labelPage2Header => "Hesap Bilgilerim";

  @override
  String get labelPage3Header => "Linklerim";

  @override
  String get labelPage4Header => "Link İstatistikleri";

  @override
  String get labelUrlNotValidMessage => "Lütfen Geçerli Bir Link Yazın";

  @override
  String get labelTokenNotValidMessage => "Lütfen Geçerli Bir Token Yazın";

  @override
  String get labelShortenButtonText => "Kısalt";

  @override
  String get labelAgreePrivacyPolicy =>
      "Devam ederek Kullanım Koşullarımızı kabul ediyor ve Gizlilik Politikamızı okuduğunuzu onaylıyorsunuz.";

  @override
  String get labelWelcome => "Hoşgeldiniz";

  @override
  String get labelEmailAddress => "Email";

  @override
  String get labelEmail => "Email Adresiniz";

  @override
  String get labelName => "İsim";

  @override
  String get labelYourName => "İsminiz";

  @override
  String get labelGetUserInfoButtonText => "Bilgilerimi Getir";

  @override
  String get labelSave => "Kaydet";

  @override
  String get apiErrorMessage =>
      "Üzgünüz, Şu Anda Link Kısaltıcı Hizmetine Erişemiyoruz";

  @override
  String get labelGetUserAllLinksButtonText => "Linklerimi Getir";

  @override
  String get labelStatus => "Durum";

  @override
  String get labelLinkStatusValid => "Geçerli";

  @override
  String get labelLinkStatusInvalid => "Geçersiz";

  @override
  String get labelExpireDate => "Son Geçerlilik Tarihi";

  @override
  String get labelPassword => "Şifre";

  @override
  String get labelDescription => "Açıklama";

  @override
  String get labelEdit => "Düzenle";

  @override
  String get labelUsageCounter => "Kullanım Sayacı";

  @override
  String get labelCurrentlyNotAvailable => "Bu Sayfa Şu Anda Mevcut Değil...";

  @override
  String get labelDeleteAccount => "Hesabı Sil";

  @override
  String get labelDelete => "Sil";
}
