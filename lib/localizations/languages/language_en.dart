import 'languages.dart';

class LanguageEn extends Languages {
  @override
  String get appName => "URL SHORTENER";

  @override
  String get labelNextLanguageName => "Türkçe";

  @override
  String get labelGetToken => "Get Token";

  @override
  String get labelShortenUrl => "Linki Kısalt";

  @override
  String get labelSelectLanguage => "Dil Seç";

  @override
  String get labelToken => "Token";

  @override
  String get labelTargetUrl => "Target Url";

  @override
  String get labelUrl => "Url";

  @override
  String get labelAbout => "About";

  @override
  String get labelAboutUs => "About Us";

  @override
  String get labelContactUs => "Contact Us";

  @override
  String get labelHelp => "Help";

  @override
  String get labelFAQ => "FAQ";

  @override
  String get labelSocial => "Social";

  @override
  String get labelLinkedin => "Linkedin";

  @override
  String get labelTrendyolBootcamp => "Trendyol Bootcamp ";

  @override
  String get labelKodluyoruzTrendyolBootcamp => "Kodluyoruz Trendyol Bootcamp ";

  @override
  String get labelGroupName => "Group 2";

  @override
  String get labelFinalProjectGroupName => "Final Project Group 2";

  @override
  String get labelCopyright2020 => "Copyright © 2020";

  @override
  String get labelDiscover => "Discover";

  @override
  String get labelPage1Header => "Shorten Url";

  @override
  String get labelPage2Header => "My Account Infos";

  @override
  String get labelPage3Header => "My Links ";

  @override
  String get labelPage4Header => "Link Statistics";

  @override
  String get labelUrlNotValidMessage => "Please Write a Valid Url";

  @override
  String get labelTokenNotValidMessage => "Please Write a Valid Token";

  @override
  String get labelShortenButtonText => "Shorten";

  @override
  String get labelAgreePrivacyPolicy =>
      "By proceeding, you agree to our Terms of Use and confirm you have read our Privacy Policy.";

  @override
  String get labelWelcome => "Welcome";

  @override
  String get labelEmailAddress => "Email Address";

  @override
  String get labelEmail => "Email";

  @override
  String get labelName => "Name";

  @override
  String get labelYourName => "Your Name";

  @override
  String get labelGetUserInfoButtonText => "Get User Info";

  @override
  String get labelSave => "Save";

  @override
  String get apiErrorMessage =>
      "Sorry Currently We Cannot Reach The Url Shortener Service";

  @override
  String get labelGetUserAllLinksButtonText => "Get My Links";

  @override
  String get labelStatus => "Status";

  @override
  String get labelLinkStatusValid => "Valid";

  @override
  String get labelLinkStatusInvalid => "Invalid";

  @override
  String get labelExpireDate => "Expire Date";

  @override
  String get labelPassword => "Password";

  @override
  String get labelDescription => "Description";

  @override
  String get labelEdit => "Edit";

  @override
  String get labelUsageCounter => "Usage Counter";

  @override
  String get labelCurrentlyNotAvailable =>
      "This Page is not Currently Available...";

  @override
  String get labelDeleteAccount => "Delete Account";

  @override
  String get labelDelete => "Delete";
}
