import 'package:flutter/material.dart';

import '../locale_constant.dart';

class LanguageData {
  static String languageCode;
  static final int languageLimit = 1;
  static int currentLanguageIndex = 0;

  static const String LANGUAGE_CODE_EN = "en";
  static const String LANGUAGE_CODE_TR = "tr";

  /*static List<LanguageData> languageList() {
    return <LanguageData>[
      LanguageData("🇺🇸", "English", 'en'),
      LanguageData("tr", "Türkçe", "tr"),
    ];
  }*/

  static void changeLanguage(BuildContext context) {
    if (currentLanguageIndex == languageLimit) {
      currentLanguageIndex = 0;
    } else {
      currentLanguageIndex = currentLanguageIndex + 1;
    }

    changeLanguageInner(context, getCurrentLanguageCode());
  }

  static String getCurrentLanguageCode() {
    switch (currentLanguageIndex) {
      case 0:
        {
          return LANGUAGE_CODE_EN;
        }
        break;

      case 1:
        {
          return LANGUAGE_CODE_TR;
        }
        break;

      default:
        {
          return LANGUAGE_CODE_EN;
        }
        break;
    }
  }
}
