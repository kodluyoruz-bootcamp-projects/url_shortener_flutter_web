import 'package:flutter/material.dart';

class LabelAndTextField extends StatefulWidget {
  LabelAndTextField({
    Key key,
    @required this.labelText,
    @required this.labelTextHint,
    @required this.textController,
    @required this.textFieldEnabled,
    @required this.validateInput,
  }) : super(key: key);

  final String labelText;
  final String labelTextHint;
  final Function() validateInput;
  final TextEditingController textController;
  final bool textFieldEnabled;
  final FocusNode textFocusNode = FocusNode();
  bool isEditing = false;

  @override
  _LabelAndTextFieldState createState() => _LabelAndTextFieldState();
}

class _LabelAndTextFieldState extends State<LabelAndTextField> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: const EdgeInsets.only(
              left: 20.0,
              bottom: 8,
            ),
            child: Text(
              widget.labelText,
              textAlign: TextAlign.left,
              style: TextStyle(
                color: Theme.of(context).textTheme.subtitle2.color,
                fontSize: 18,
                // fontFamily: 'Montserrat',
                fontWeight: FontWeight.bold,
                // letterSpacing: 3,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              left: 20.0,
              right: 20,
            ),
            child: TextField(
              enabled: widget.textFieldEnabled,
              focusNode: widget.textFocusNode,
              keyboardType: TextInputType.text,
              textInputAction: TextInputAction.next,
              controller: widget.textController,
              autofocus: false,
              onChanged: (value) {
                setState(() {
                  widget.isEditing = true;
                });
              },
              onSubmitted: (value) {
                widget.textFocusNode.unfocus();
              },
              style: TextStyle(color: Colors.black),
              decoration: InputDecoration(
                border: new OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: BorderSide(
                    color: Colors.blueGrey[800],
                    width: 3,
                  ),
                ),
                filled: true,
                hintStyle: new TextStyle(
                  color: Colors.blueGrey[300],
                ),
                hintText: widget.labelTextHint,
                fillColor: Colors.white,
                errorText: widget.isEditing ? widget.validateInput() : null,
                errorStyle: TextStyle(
                  fontSize: 12,
                  color: Colors.redAccent,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
