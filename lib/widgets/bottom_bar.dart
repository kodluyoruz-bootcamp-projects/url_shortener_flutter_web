import 'package:url_shortener_flutter_web/localizations/languages/languages.dart';
import 'package:url_shortener_flutter_web/widgets/bottom_bar_column.dart';
import 'package:url_shortener_flutter_web/widgets/info_text.dart';
import 'package:url_shortener_flutter_web/widgets/responsive.dart';
import 'package:flutter/material.dart';

class BottomBar extends StatelessWidget {
  const BottomBar({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(30),
      color: Theme.of(context).bottomAppBarColor,
      child: ResponsiveWidget.isSmallScreen(context)
          ? Column(
              children: [
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    BottomBarColumn(
                      heading: Languages.of(context).labelAbout.toUpperCase(),
                      s1: Languages.of(context).labelAboutUs,
                      s2: Languages.of(context).labelContactUs,
                      s3: '',
                    ),
                    BottomBarColumn(
                      heading: Languages.of(context).labelHelp.toUpperCase(),
                      s1: Languages.of(context).labelFAQ,
                      s2: '',
                      s3: '',
                    ),
                    BottomBarColumn(
                      heading: Languages.of(context).labelSocial.toUpperCase(),
                      s1: Languages.of(context).labelLinkedin,
                      s2: '',
                      s3: '',
                    ),
                  ],
                ),
                Container(
                  color: Colors.blueGrey,
                  width: double.maxFinite,
                  height: 1,
                ),
                SizedBox(height: 20),
                InfoText(
                  type: Languages.of(context).labelKodluyoruzTrendyolBootcamp,
                  text: Languages.of(context).labelGroupName,
                ),
                SizedBox(height: 5),
                SizedBox(height: 20),
                Container(
                  color: Colors.blueGrey,
                  width: double.maxFinite,
                  height: 1,
                ),
                SizedBox(height: 20),
                Text(
                  Languages.of(context).labelCopyright2020 +
                      ' | ' +
                      Languages.of(context).labelGroupName +
                      ' ' +
                      Languages.of(context).labelTrendyolBootcamp,
                  style: TextStyle(
                    color: Colors.blueGrey[300],
                    fontSize: 14,
                  ),
                ),
              ],
            )
          : Column(
              children: [
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    BottomBarColumn(
                      heading: Languages.of(context).labelAbout.toUpperCase(),
                      s1: Languages.of(context).labelAboutUs,
                      s2: Languages.of(context).labelContactUs,
                      s3: '',
                    ),
                    BottomBarColumn(
                      heading: Languages.of(context).labelHelp.toUpperCase(),
                      s1: Languages.of(context).labelFAQ,
                      s2: '',
                      s3: '',
                    ),
                    BottomBarColumn(
                      heading: Languages.of(context).labelSocial.toUpperCase(),
                      s1: Languages.of(context).labelLinkedin,
                      s2: '',
                      s3: '',
                    ),
                    Container(
                      color: Colors.blueGrey,
                      width: 2,
                      height: 150,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        InfoText(
                          type: Languages.of(context).labelTrendyolBootcamp,
                          text: Languages.of(context).labelGroupName,
                        ),
                        SizedBox(height: 5)
                      ],
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    color: Colors.blueGrey,
                    width: double.maxFinite,
                    height: 1,
                  ),
                ),
                SizedBox(height: 20),
                Text(
                  Languages.of(context).labelCopyright2020 +
                      ' | ' +
                      Languages.of(context).labelTrendyolBootcamp +
                      ' ' +
                      Languages.of(context).labelGroupName,
                  style: TextStyle(
                    color: Colors.blueGrey[300],
                    fontSize: 14,
                  ),
                ),
              ],
            ),
    );
  }
}
