import 'dart:async';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:flutter/material.dart';

class LoadingAnimationButton extends StatefulWidget {
  const LoadingAnimationButton({
    Key key,
    @required this.buttonText,
    @required this.color,
    @required this.onPressedMethod,
    this.extraMethod,
  }) : super(key: key);

  final String buttonText;
  final Color color;
  final Function() onPressedMethod;
  final VoidCallback extraMethod;

  @override
  _LoadingAnimationButtonState createState() => _LoadingAnimationButtonState();
}

class _LoadingAnimationButtonState extends State<LoadingAnimationButton> {
  final RoundedLoadingButtonController _btnController =
      new RoundedLoadingButtonController();

  void _doButtonStartState() async {
    Timer(Duration(seconds: 2), () {
      if (widget.extraMethod != null) {
        widget.extraMethod();
      } else {
        _btnController.stop();
      }
    });
  }

  void _doButtonErrorState() async {
    setState(() {
      _btnController.error();
    });
  }

  void _doButtonSuccessState() async {
    setState(() {
      _btnController.success();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          RoundedLoadingButton(
            child: Text(widget.buttonText,
                style: TextStyle(fontSize: 14, color: Colors.white)),
            controller: _btnController,
            color: widget.color,
            onPressed: () async {
              bool result = await widget.onPressedMethod();

              print("onPressedMethod Response " + result.toString());
              if (result == true) {
                _doButtonSuccessState();
              } else {
                _doButtonErrorState();
              }
              _doButtonStartState();
            },
          )
        ],
      ),
    );
  }
}
