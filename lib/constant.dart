import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xFFFFC200);
const kTextcolor = Color(0xFF241424);
const kDarkButton = Color(0xFF372930);
final kButtonColor = Color(Colors.blueGrey[800].value);

final kPrimaryButtonColor = Color(Colors.blue.shade800.value);
final kSecondaryButtonColor = Color(Colors.amber.shade600.value);
final kDangerZoneButtonColor = Color(Colors.redAccent.shade400.value);

const API_SHORTENER_IP = "http://8.208.85.44:30132/v1";
const API_SHORTENER_IP_GET_USER = "/users";
const API_SHORTENER_IP_GET_USER_LINKS = "/links";
const API_REDIRECTION_IP = "http://8.208.85.44:30133/";
const OPENING_PAGE_INDEX = 0;
