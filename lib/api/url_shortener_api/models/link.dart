import 'package:flutter/material.dart';
import 'package:url_shortener_flutter_web/localizations/languages/languages.dart';

import '../../../constant.dart';

class Link {
  String id;
  String shortLink;
  String targetLink;
  String password;
  DateTime expireDate;
  String status;
  Color color;
  String description;
  List<String> tags;
  int usageCounter;

  Link(
      {this.id,
      this.shortLink,
      this.targetLink,
      this.password,
      this.expireDate,
      this.status,
      this.color,
      this.description,
      this.tags,
      this.usageCounter});

  factory Link.fromJson(dynamic json, BuildContext context) {
    return Link(
      id: json['id'],
      shortLink: Link.getShortLink(json['id'], context),
      targetLink: json['targetLink'],
      password: json['password'],
      expireDate: json['expireDate'],
      status: Link.getStatus(json['expireDate'], context),
      color: Link.getColor(json['expireDate'], context),
      description: json['description'],
      tags: json['tags'],
      usageCounter: json['usageCounter'],
    );
  }

  Link.createSimpleLink(this.id, this.targetLink) {
    this.password = "";
    this.expireDate = DateTime.utc(2099, DateTime.november, 9);
    this.description = "";
    this.tags = List();
  }

  Link.createLink(String id, String targetLink, String password,
      DateTime expireDate, String description, List<String> tags) {
    this.id = id;
    this.targetLink = targetLink;
    this.password = password;
    this.expireDate = expireDate;
    this.description = description;
    this.tags = tags;
  }

  static String getStatus(String json, BuildContext context) {
    if (json == null) {
      return Languages.of(context).labelLinkStatusValid;
    }
    return Languages.of(context).labelLinkStatusInvalid;
  }

  static Color getColor(String json, BuildContext context) {
    if (json == null) {
      return Colors.green;
    }
    return Colors.red;
  }

  static String getShortLink(String json, BuildContext context) {
    return API_REDIRECTION_IP + json;
  }
}
