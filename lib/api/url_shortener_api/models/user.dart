import 'package:url_shortener_flutter_web/api/url_shortener_api/models/link.dart';

class User {
  String name;
  String email;
  String token;

  User({this.name, this.email, this.token});

  factory User.fromJson(dynamic json) {
    return User(name: json['name'], email: json['email'], token: json['token']);
  }

  User.createSimpleUser(String name, String email) {
    this.name = name;
    this.email = email;
  }

  User.createUser(String name, String email, String token, List<Link> links,
      int linkCount) {
    this.name = name;
    this.email = email;
    this.token = token;
  }
}
