import 'package:flutter/material.dart';
import 'package:url_shortener_flutter_web/api/url_shortener_api/models/link.dart';
import 'package:url_shortener_flutter_web/api/url_shortener_api/models/user.dart';
import 'package:url_shortener_flutter_web/api/url_shortener_api/repositories/url_shortener_api_real_repository.dart';
import 'package:url_shortener_flutter_web/api/url_shortener_api/repositories/url_shortener_api_repository.dart';
import 'package:url_shortener_flutter_web/api/url_shortener_api/repositories/url_shortener_api_success_mock_repository.dart';

class URLShortenerAPIService {
  URLShortenerAPIRepository urlShortenerAPIRepository =
      URLShortenerAPIRealRepository();

  Future<String> createLink(String token, String url) async {
    return urlShortenerAPIRepository.createLink(token, url);
  }

  Future<String> createToken(String email, String name) async {
    return urlShortenerAPIRepository.createToken(email, name);
  }

  Future<User> getUserInfo(String token) async {
    return urlShortenerAPIRepository.getUserInfo(token);
  }

  Future<Link> getUserLink(
      String token, String linkId, BuildContext context) async {
    return urlShortenerAPIRepository.getUserLink(token, linkId, context);
  }

  Future<List<Link>> getUserAllLinks(String token, BuildContext context) async {
    return urlShortenerAPIRepository.getUserAllLinks(token, context);
  }

  Future<bool> updateLink(String token, Link link) async {
    return urlShortenerAPIRepository.updateLink(token, link);
  }

  Future<bool> updateUser(String token, String name) async {
    return urlShortenerAPIRepository.updateUser(token, name);
  }

  Future<bool> deleteUser(String token) async {
    return urlShortenerAPIRepository.deleteUser(token);
  }

  Future<bool> deleteLink(String token, String linkId) async {
    return urlShortenerAPIRepository.deleteLink(token, linkId);
  }
}
