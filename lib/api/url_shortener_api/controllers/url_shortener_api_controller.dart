import 'package:flutter/material.dart';
import 'package:url_shortener_flutter_web/api/url_shortener_api/models/link.dart';
import 'package:url_shortener_flutter_web/api/url_shortener_api/models/user.dart';
import 'package:url_shortener_flutter_web/api/url_shortener_api/services/url_shortener_api_service.dart';

class URLShortenerAPIController {
  static URLShortenerAPIService urlShortenerAPIService =
      URLShortenerAPIService();

  static Future<String> createLink(String token, String url) async {
    return urlShortenerAPIService.createLink(token, url);
  }

  static Future<String> createToken(String email, String name) async {
    return urlShortenerAPIService.createToken(email, name);
  }

  static Future<User> getUserInfo(String token) async {
    return urlShortenerAPIService.getUserInfo(token);
  }

  static Future<Link> getUserLink(
      String token, String linkId, BuildContext context) async {
    return urlShortenerAPIService.getUserLink(token, linkId, context);
  }

  static Future<List<Link>> getUserAllLinks(
      String token, BuildContext context) async {
    return urlShortenerAPIService.getUserAllLinks(token, context);
  }

  static Future<bool> updateLink(String token, Link link) async {
    return urlShortenerAPIService.updateLink(token, link);
  }

  static Future<bool> updateUser(String token, String name) async {
    return urlShortenerAPIService.updateUser(token, name);
  }

  static Future<bool> deleteUser(String token) async {
    return urlShortenerAPIService.deleteUser(token);
  }

  static Future<bool> deleteLink(String token, String linkId) async {
    return urlShortenerAPIService.deleteLink(token, linkId);
  }
}
