import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:url_shortener_flutter_web/api/url_shortener_api/models/link.dart';
import 'package:url_shortener_flutter_web/api/url_shortener_api/models/user.dart';
import 'package:url_shortener_flutter_web/api/url_shortener_api/repositories/url_shortener_api_repository.dart';

import '../../../constant.dart';

class URLShortenerAPIRealRepository implements URLShortenerAPIRepository {
  Response response;
  Dio dio = new Dio();

  Future<String> createLink(String token, String url) async {
    try {
      response = await dio.post(
          API_SHORTENER_IP +
              API_SHORTENER_IP_GET_USER +
              "/" +
              token +
              API_SHORTENER_IP_GET_USER_LINKS,
          data: {"targetLink": url});

      Headers headers = response.headers;
      String location = headers["location"].toString();
      String shortLinkPath = location?.split("/")?.last;
      return shortLinkPath.substring(0, shortLinkPath.length - 1);
    } catch (e) {
      return null;
    }
  }

  Future<String> createToken(String email, String name) async {
    response = await dio.post(API_SHORTENER_IP + API_SHORTENER_IP_GET_USER,
        data: {"email": email, "name": name});

    try {
      Headers headers = response.headers;
      String location = headers["location"].toString();
      String tokenPath = location?.split("/")?.last;
      return tokenPath.substring(0, tokenPath.length - 1);
    } catch (e) {
      return null;
    }
  }

  Future<User> getUserInfo(String token) async {
    try {
      response = await dio
          .get(API_SHORTENER_IP + API_SHORTENER_IP_GET_USER + "/" + token);
      if (response.statusCode == 200) {
        return User.fromJson(response.data);
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future<Link> getUserLink(
      String token, String linkId, BuildContext context) async {
    try {
      response = await dio.get(API_SHORTENER_IP +
          API_SHORTENER_IP_GET_USER +
          "/" +
          token +
          API_SHORTENER_IP_GET_USER_LINKS +
          "/" +
          linkId);
      if (response.statusCode == 200) {
        return Link.fromJson(response.data, context);
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future<List<Link>> getUserAllLinks(String token, BuildContext context) async {
    try {
      response = await dio.get(API_SHORTENER_IP +
          API_SHORTENER_IP_GET_USER +
          "/" +
          token +
          API_SHORTENER_IP_GET_USER_LINKS);

      if (response.statusCode == 200) {
        List<dynamic> dynamicList = response.data;
        List<Link> links = List<Link>();

        for (int i = 0; i < dynamicList.length; i++) {
          links.add(new Link.fromJson(dynamicList[i], context));
        }

        return links;
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future<bool> updateLink(String token, Link link) async {
    response = await dio.patch(
        API_SHORTENER_IP +
            API_SHORTENER_IP_GET_USER +
            "/" +
            token +
            API_SHORTENER_IP_GET_USER_LINKS +
            "/" +
            link.id,
        data: {"description": link.description, "password": link.password});

    if (response.statusCode == 204) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> updateUser(String token, String name) async {
    response = await dio.patch(
        API_SHORTENER_IP + API_SHORTENER_IP_GET_USER + "/" + token,
        data: {"name": name});

    if (response.statusCode == 204) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> deleteUser(String token) async {
    response = await dio
        .delete(API_SHORTENER_IP + API_SHORTENER_IP_GET_USER + "/" + token);

    if (response.statusCode == 204) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> deleteLink(String token, String linkId) async {
    response = await dio.delete(API_SHORTENER_IP +
        API_SHORTENER_IP_GET_USER +
        "/" +
        token +
        API_SHORTENER_IP_GET_USER_LINKS +
        "/" +
        linkId);

    if (response.statusCode == 204) {
      return true;
    } else {
      return false;
    }
  }
}
