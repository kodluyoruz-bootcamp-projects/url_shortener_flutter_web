import 'package:flutter/material.dart';
import 'package:url_shortener_flutter_web/api/url_shortener_api/models/link.dart';
import 'package:url_shortener_flutter_web/api/url_shortener_api/models/user.dart';
import 'package:url_shortener_flutter_web/api/url_shortener_api/repositories/url_shortener_api_repository.dart';

class URLShortenerAPISuccessMockRepository
    implements URLShortenerAPIRepository {
  Future<String> createLink(String token, String url) async {
    return "b7303fd6";
  }

  Future<String> createToken(String email, String name) async {
    return "111f4388";
  }

  Future<User> getUserInfo(String token) async {
    return User.createSimpleUser("ahmet", "ahmet@gmail.com");
  }

  Future<Link> getUserLink(
      String token, String linkId, BuildContext context) async {
    return Link.createSimpleLink("14444", "www.google.com");
  }

  Future<List<Link>> getUserAllLinks(String token, BuildContext context) async {
    List<Link> links = List();
    links.add(Link.createSimpleLink("b7303fd6", "www.google.com"));
    List<String> tags = List();
    tags.add("blue");
    tags.add("red");
    tags.add("yellow");
    links.add(Link.createLink("avdv445c", "www.ahmet.com", "1453",
        DateTime.now(), "Best Link ", tags));

    return links;
  }

  Future<bool> updateLink(String token, Link link) async {
    return true;
  }

  Future<bool> updateUser(String token, String name) async {
    return true;
  }

  Future<bool> deleteUser(String token) async {
    return true;
  }

  Future<bool> deleteLink(String token, String linkId) async {
    return true;
  }
}
