import 'package:flutter/material.dart';
import 'package:url_shortener_flutter_web/api/url_shortener_api/models/link.dart';
import 'package:url_shortener_flutter_web/api/url_shortener_api/models/user.dart';

abstract class URLShortenerAPIRepository {
  Future<String> createLink(String token, String url);

  Future<String> createToken(String email, String name);

  Future<User> getUserInfo(String token);

  Future<Link> getUserLink(String token, String linkId, BuildContext context);

  Future<List<Link>> getUserAllLinks(String token, BuildContext context);

  Future<bool> updateLink(String token, Link link);

  Future<bool> updateUser(String token, String name);

  Future<bool> deleteUser(String token);

  Future<bool> deleteLink(String token, String linkId);
}
