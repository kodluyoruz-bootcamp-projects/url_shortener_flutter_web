import 'package:flutter/material.dart';
import 'package:url_shortener_flutter_web/api/url_shortener_api/models/link.dart';
import 'package:url_shortener_flutter_web/api/url_shortener_api/models/user.dart';
import 'package:url_shortener_flutter_web/api/url_shortener_api/repositories/url_shortener_api_repository.dart';

class URLShortenerAPIFailMockRepository implements URLShortenerAPIRepository {
  Future<String> createLink(String token, String url) async {
    return null;
  }

  Future<String> createToken(String email, String name) async {
    return null;
  }

  Future<User> getUserInfo(String token) async {
    return null;
  }

  Future<Link> getUserLink(
      String token, String linkId, BuildContext context) async {
    return null;
  }

  Future<List<Link>> getUserAllLinks(String token, BuildContext context) async {
    return null;
  }

  Future<bool> updateLink(String token, Link link) async {
    return false;
  }

  Future<bool> updateUser(String token, String name) async {
    return false;
  }

  Future<bool> deleteUser(String token) async {
    return false;
  }

  Future<bool> deleteLink(String token, String linkId) async {
    return false;
  }
}
