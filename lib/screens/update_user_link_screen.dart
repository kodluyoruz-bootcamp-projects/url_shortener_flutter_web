import 'package:url_shortener_flutter_web/api/url_shortener_api/controllers/url_shortener_api_controller.dart';
import 'package:url_shortener_flutter_web/api/url_shortener_api/models/link.dart';
import 'package:url_shortener_flutter_web/constant.dart';
import 'package:url_shortener_flutter_web/localizations/languages/languages.dart';
import 'package:flutter/material.dart';
import 'package:url_shortener_flutter_web/widgets/label_end_text_field.dart';
import 'package:url_shortener_flutter_web/widgets/loading.dart';
import 'package:url_shortener_flutter_web/widgets/loading_animation_button.dart';

class UpdateUserLinkScreen extends StatefulWidget {
  const UpdateUserLinkScreen({
    Key key,
    @required this.screenSize,
    @required this.token,
    @required this.linkId,
  }) : super(key: key);

  final Size screenSize;
  final String token;
  final String linkId;

  @override
  _UpdateUserLinkScreenState createState() => _UpdateUserLinkScreenState();
}

class _UpdateUserLinkScreenState extends State<UpdateUserLinkScreen> {
  Link link;

  TextEditingController textControllerDate;
  FocusNode textFocusNodeDate;

  TextEditingController textControllerPassword;
  FocusNode textFocusNodePassword;

  TextEditingController textControllerDescription;
  FocusNode textFocusNodeDescription;

  Future<bool> updateList() async {
    //String date = textControllerDate.text.trim();
    String password = textControllerPassword.text.trim();
    String description = textControllerDescription.text.trim();

    //link.date = date;
    link.password = password;
    link.description = description;

    try {
      return await URLShortenerAPIController.updateLink(widget.token, link);
    } catch (e) {
      return false;
    }
  }

  Future<void> setLinkDetails() async {
    link = await URLShortenerAPIController.getUserLink(
        widget.token, widget.linkId, context);

    if (link != null) {
      setState(() {
        if (link.expireDate != null) {
          textControllerDate.text = link.expireDate.toString();
        }
        textControllerPassword.text = link.password;
        textControllerDescription.text = link.description;
      });
    } else {
      navigatorPop();
    }
  }

  String validateTrue() {
    return null;
  }

  void navigatorPop() {
    Navigator.pop(context);
  }

  @override
  void initState() {
    textControllerDate = TextEditingController();
    textControllerPassword = TextEditingController();
    textControllerDescription = TextEditingController();
    textFocusNodeDate = FocusNode();
    textFocusNodePassword = FocusNode();
    textFocusNodeDescription = FocusNode();
    setLinkDetails();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: Theme.of(context).backgroundColor,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8.0),
      ),
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Container(
            width: 400,
            color: Theme.of(context).backgroundColor,
            child: link == null
                ? Loading()
                : Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Center(
                        child: Text(
                          link.shortLink,
                          style: TextStyle(
                            color: Theme.of(context).textTheme.headline1.color,
                            fontSize: 11,
                            fontFamily: 'Montserrat',
                            fontWeight: FontWeight.bold,
                            letterSpacing: 3,
                          ),
                        ),
                      ),
                      SizedBox(height: 30),
                      LabelAndTextField(
                          labelText: Languages.of(context).labelExpireDate,
                          labelTextHint: Languages.of(context).labelExpireDate,
                          textController: textControllerDate,
                          textFieldEnabled: true,
                          validateInput: validateTrue),
                      SizedBox(height: 20),
                      LabelAndTextField(
                          labelText: Languages.of(context).labelPassword,
                          labelTextHint: Languages.of(context).labelPassword,
                          textController: textControllerPassword,
                          textFieldEnabled: true,
                          validateInput: validateTrue),
                      SizedBox(height: 20),
                      LabelAndTextField(
                          labelText: Languages.of(context).labelDescription,
                          labelTextHint: Languages.of(context).labelDescription,
                          textController: textControllerDescription,
                          textFieldEnabled: true,
                          validateInput: validateTrue),
                      SizedBox(height: 20),
                      Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Flexible(
                              flex: 1,
                              child: Container(
                                width: double.maxFinite,
                                child: LoadingAnimationButton(
                                    buttonText: Languages.of(context).labelSave,
                                    color: kSecondaryButtonColor,
                                    onPressedMethod: updateList,
                                    extraMethod: navigatorPop),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
          ),
        ),
      ),
    );
  }
}
