import 'package:url_shortener_flutter_web/localizations/languages/languages.dart';
import 'package:flutter/material.dart';

import '../constant.dart';

class GetUserLinksStatisticsScreen extends StatefulWidget {
  const GetUserLinksStatisticsScreen({
    Key key,
    @required this.screenSize,
  }) : super(key: key);

  final Size screenSize;

  @override
  _GetUserLinksStatisticsScreenState createState() =>
      _GetUserLinksStatisticsScreenState();
}

class _GetUserLinksStatisticsScreenState
    extends State<GetUserLinksStatisticsScreen> {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: Theme.of(context).backgroundColor,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8.0),
      ),
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Container(
            width: 400,
            color: Theme.of(context).backgroundColor,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(height: 30),
                Center(
                  child: Padding(
                    padding: EdgeInsets.only(
                      top: 15.0,
                      bottom: 15.0,
                    ),
                    child: Text(
                      Languages.of(context).labelCurrentlyNotAvailable,
                      style: TextStyle(
                        fontSize: 14,
                        color: Colors.red,
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 30),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
