import 'package:url_shortener_flutter_web/api/url_shortener_api/controllers/url_shortener_api_controller.dart';
import 'package:url_shortener_flutter_web/constant.dart';
import 'package:url_shortener_flutter_web/localizations/languages/languages.dart';
import 'package:flutter/material.dart';
import 'package:url_shortener_flutter_web/widgets/loading_animation_button.dart';

class GetTokenScreen extends StatefulWidget {
  const GetTokenScreen({
    Key key,
    @required this.screenSize,
    @required this.newUserTokenController,
    @required this.saveLocalTokenMethod,
  }) : super(key: key);

  final Size screenSize;
  final TextEditingController newUserTokenController;
  final Function saveLocalTokenMethod;

  @override
  _GetTokenScreenState createState() => _GetTokenScreenState();
}

class _GetTokenScreenState extends State<GetTokenScreen> {
  TextEditingController textControllerEmail;
  FocusNode textFocusNodeEmail;
  bool _isEditingEmail = false;

  TextEditingController textControllerName;
  FocusNode textFocusNodeName;
  bool _isEditingName = false;

  String _validateEmail() {
    String value = textControllerEmail.text.trim();

    if (textControllerEmail.text != null) {
      if (value.isEmpty) {
        return 'Email can\'t be empty';
      } else if (!value.contains(RegExp(
          r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+"))) {
        return 'Enter a correct email address';
      }
    }

    return null;
  }

  bool isValidateEmail() {
    if (_validateEmail() == null) {
      return true;
    }
    return false;
  }

  String _validateName() {
    String value = textControllerName.text.trim();

    if (value != null) {
      if (value.isEmpty) {
        return 'Name can\'t be empty';
      } else if (value.length < 3) {
        return 'Length of name should be greater than 3';
      }
    }

    return null;
  }

  bool isValidateName() {
    if (_validateName() == null) {
      return true;
    }
    return false;
  }

  Future<bool> createToken() async {
    String email = textControllerEmail.text.trim();
    String name = textControllerName.text.trim();

    if (isValidateEmail() && isValidateName()) {
      String result = await URLShortenerAPIController.createToken(email, name);

      if (result != null) {
        setState(() {
          widget.newUserTokenController.text = result;
          widget.saveLocalTokenMethod(result);
        });
        return true;
      }
    }
    return false;
  }

  void navigatorPop() {
    Navigator.pop(context);
  }

  @override
  void initState() {
    textControllerEmail = TextEditingController();
    textControllerName = TextEditingController();
    textControllerEmail.text = null;
    textControllerName.text = null;
    textFocusNodeEmail = FocusNode();
    textFocusNodeName = FocusNode();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: Theme.of(context).backgroundColor,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8.0),
      ),
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Container(
            width: 400,
            color: Theme.of(context).backgroundColor,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Center(
                  child: Text(
                    Languages.of(context).appName,
                    style: TextStyle(
                      color: Theme.of(context).textTheme.headline1.color,
                      fontSize: 24,
                      fontFamily: 'Montserrat',
                      fontWeight: FontWeight.bold,
                      letterSpacing: 3,
                    ),
                  ),
                ),
                SizedBox(height: 30),
                Padding(
                  padding: const EdgeInsets.only(
                    left: 20.0,
                    bottom: 8,
                  ),
                  child: Text(
                    Languages.of(context).labelEmailAddress,
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      color: Theme.of(context).textTheme.subtitle2.color,
                      fontSize: 18,
                      // fontFamily: 'Montserrat',
                      fontWeight: FontWeight.bold,
                      // letterSpacing: 3,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    left: 20.0,
                    right: 20,
                  ),
                  child: TextField(
                    focusNode: textFocusNodeEmail,
                    keyboardType: TextInputType.emailAddress,
                    textInputAction: TextInputAction.next,
                    controller: textControllerEmail,
                    autofocus: false,
                    onChanged: (value) {
                      setState(() {
                        _isEditingEmail = true;
                      });
                    },
                    onSubmitted: (value) {
                      textFocusNodeEmail.unfocus();
                      FocusScope.of(context).requestFocus(textFocusNodeName);
                    },
                    style: TextStyle(color: Colors.black),
                    decoration: InputDecoration(
                      border: new OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: BorderSide(
                          color: Colors.blueGrey[800],
                          width: 3,
                        ),
                      ),
                      filled: true,
                      hintStyle: new TextStyle(
                        color: Colors.blueGrey[300],
                      ),
                      hintText: Languages.of(context).labelEmail,
                      fillColor: Colors.white,
                      errorText: _isEditingEmail ? _validateEmail() : null,
                      errorStyle: TextStyle(
                        fontSize: 12,
                        color: Colors.redAccent,
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 20),
                Padding(
                  padding: const EdgeInsets.only(
                    left: 20.0,
                    bottom: 8,
                  ),
                  child: Text(
                    Languages.of(context).labelName,
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      color: Theme.of(context).textTheme.subtitle2.color,
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                      // letterSpacing: 3,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    left: 20.0,
                    right: 20,
                  ),
                  child: TextField(
                    focusNode: textFocusNodeName,
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.done,
                    controller: textControllerName,
                    autofocus: false,
                    onChanged: (value) {
                      setState(() {
                        _isEditingName = true;
                      });
                    },
                    onSubmitted: (value) {
                      textFocusNodeName.unfocus();
                      FocusScope.of(context).requestFocus(textFocusNodeName);
                    },
                    style: TextStyle(color: Colors.black),
                    decoration: InputDecoration(
                      border: new OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: BorderSide(
                          color: Colors.blueGrey[800],
                          width: 3,
                        ),
                      ),
                      filled: true,
                      hintStyle: new TextStyle(
                        color: Colors.blueGrey[300],
                      ),
                      hintText: Languages.of(context).labelYourName,
                      fillColor: Colors.white,
                      errorText: _isEditingName ? _validateName() : null,
                      errorStyle: TextStyle(
                        fontSize: 12,
                        color: Colors.redAccent,
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Flexible(
                        flex: 1,
                        child: Container(
                          width: double.maxFinite,
                          child: LoadingAnimationButton(
                              buttonText: Languages.of(context).labelGetToken,
                              color: kSecondaryButtonColor,
                              onPressedMethod: createToken,
                              extraMethod: navigatorPop),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    left: 40.0,
                    right: 40.0,
                  ),
                  child: Container(
                    height: 1,
                    width: double.maxFinite,
                    color: Colors.blueGrey[200],
                  ),
                ),
                SizedBox(height: 30),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    Languages.of(context).labelAgreePrivacyPolicy,
                    maxLines: 2,
                    style: TextStyle(
                      color: Theme.of(context).textTheme.subtitle2.color,
                      fontSize: 14,
                      fontWeight: FontWeight.w300,
                      // letterSpacing: 3,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
