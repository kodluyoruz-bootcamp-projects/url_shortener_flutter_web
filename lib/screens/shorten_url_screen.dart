import 'dart:html';

import 'package:animated_icon_button/animated_icon_button.dart';
import 'package:clipboard/clipboard.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:url_shortener_flutter_web/api/url_shortener_api/controllers/url_shortener_api_controller.dart';
import 'package:url_shortener_flutter_web/localizations/languages/languages.dart';
import 'package:url_shortener_flutter_web/screens/get_token_screen.dart';
import 'package:flutter/material.dart';
import 'package:url_shortener_flutter_web/utils/url_launcher_helper.dart';
import 'package:url_shortener_flutter_web/widgets/loading_animation_button.dart';

import 'package:validators/validators.dart';

import '../constant.dart';

class ShortenUrlScreen extends StatefulWidget {
  const ShortenUrlScreen({
    Key key,
    @required this.screenSize,
  }) : super(key: key);

  final Size screenSize;

  @override
  _ShortenUrlScreenState createState() => _ShortenUrlScreenState();
}

class _ShortenUrlScreenState extends State<ShortenUrlScreen> {
  TextEditingController textControllerToken;
  FocusNode textFocusNodeToken;
  bool _isEditingToken = false;

  TextEditingController textControllerUrl;
  FocusNode textFocusNodeUrl;
  bool _isEditingUrl = false;

  TextEditingController textControllerCurrentShortenUrl;

  final Storage _localTokenStorage = window.localStorage;

  bool isShortLinkAndCopyVisible;

  Future<void> setLocalTokenStorage() async {
    textControllerToken.text = _localTokenStorage['_localTokenStorage'];
  }

  Future<void> saveLocalTokenStorage(String localTokenStorage) async {
    _localTokenStorage['_localTokenStorage'] = localTokenStorage;
  }

  String _validateToken() {
    if (textControllerToken.text.trim().isEmpty) {
      return Languages.of(context).labelTokenNotValidMessage;
    }

    return null;
  }

  bool isValidateToken() {
    if (_validateToken() == null) {
      return true;
    }
    return false;
  }

  String _validateUrl() {
    if (!isURL(textControllerUrl.text.trim())) {
      return Languages.of(context).labelUrlNotValidMessage;
    }

    return null;
  }

  bool isValidateUrl() {
    if (_validateUrl() == null) {
      return true;
    }
    return false;
  }

  @override
  void initState() {
    textControllerToken = TextEditingController();
    textControllerUrl = TextEditingController();
    textControllerToken.text = null;
    setLocalTokenStorage();
    textControllerUrl.text = null;
    textFocusNodeToken = FocusNode();
    textFocusNodeUrl = FocusNode();
    textControllerCurrentShortenUrl = TextEditingController();
    textControllerCurrentShortenUrl.text = null;
    isShortLinkAndCopyVisible = false;
    super.initState();
  }

  Future<bool> createShortenUrl() async {
    String token = textControllerToken.text.trim();
    String url = textControllerUrl.text.trim();
    saveLocalTokenStorage(token);

    if (isValidateToken() && isValidateUrl()) {
      String result = await URLShortenerAPIController.createLink(token, url);

      if (result != null && result != "") {
        setState(() {
          textControllerCurrentShortenUrl.text = API_REDIRECTION_IP + result;
          isShortLinkAndCopyVisible = true;
        });
        return true;
      }
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: Theme.of(context).backgroundColor,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8.0),
      ),
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Container(
            width: 400,
            color: Theme.of(context).backgroundColor,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(height: 30),
                Padding(
                  padding: const EdgeInsets.only(
                    left: 20.0,
                    bottom: 8,
                  ),
                  child: Text(
                    Languages.of(context).labelToken,
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      color: Theme.of(context).textTheme.subtitle2.color,
                      fontSize: 18,
                      // fontFamily: 'Montserrat',
                      fontWeight: FontWeight.bold,
                      // letterSpacing: 3,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    left: 20.0,
                    right: 20,
                  ),
                  child: TextField(
                    focusNode: textFocusNodeToken,
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.next,
                    controller: textControllerToken,
                    autofocus: false,
                    onChanged: (value) {
                      setState(() {
                        _isEditingToken = true;
                      });
                    },
                    onSubmitted: (value) {
                      textFocusNodeToken.unfocus();
                      FocusScope.of(context).requestFocus(textFocusNodeUrl);
                    },
                    style: TextStyle(color: Colors.black),
                    decoration: InputDecoration(
                      border: new OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: BorderSide(
                          color: Colors.blueGrey[800],
                          width: 3,
                        ),
                      ),
                      filled: true,
                      hintStyle: new TextStyle(
                        color: Colors.blueGrey[300],
                      ),
                      hintText: Languages.of(context).labelToken,
                      fillColor: Colors.white,
                      errorText: _isEditingUrl ? _validateToken() : null,
                      errorStyle: TextStyle(
                        fontSize: 12,
                        color: Colors.redAccent,
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 20),
                Padding(
                  padding: const EdgeInsets.only(
                    left: 20.0,
                    bottom: 8,
                  ),
                  child: Text(
                    Languages.of(context).labelTargetUrl,
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      color: Theme.of(context).textTheme.subtitle2.color,
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                      // letterSpacing: 3,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    left: 20.0,
                    right: 20,
                  ),
                  child: TextField(
                    focusNode: textFocusNodeUrl,
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.done,
                    controller: textControllerUrl,
                    autofocus: false,
                    onChanged: (value) {
                      setState(() {
                        _isEditingUrl = true;
                      });
                    },
                    onSubmitted: (value) {
                      textFocusNodeUrl.unfocus();
                      FocusScope.of(context).requestFocus(textFocusNodeUrl);
                    },
                    style: TextStyle(color: Colors.black),
                    decoration: InputDecoration(
                      border: new OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: BorderSide(
                          color: Colors.blueGrey[800],
                          width: 3,
                        ),
                      ),
                      filled: true,
                      hintStyle: new TextStyle(
                        color: Colors.blueGrey[300],
                      ),
                      hintText: Languages.of(context).labelTargetUrl,
                      fillColor: Colors.white,
                      errorText: _isEditingUrl ? _validateUrl() : null,
                      errorStyle: TextStyle(
                        fontSize: 12,
                        color: Colors.redAccent,
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Flexible(
                        flex: 1,
                        child: Container(
                          width: double.maxFinite,
                          child: LoadingAnimationButton(
                              buttonText:
                                  Languages.of(context).labelShortenButtonText,
                              color: kPrimaryButtonColor,
                              onPressedMethod: createShortenUrl),
                        ),
                      ),
                      SizedBox(width: 20),
                      Flexible(
                        flex: 1,
                        child: Container(
                          width: double.maxFinite,
                          child: FlatButton(
                            color: kSecondaryButtonColor,
                            hoverColor: kSecondaryButtonColor,
                            highlightColor: Colors.black,
                            onPressed: () async {
                              showDialog(
                                context: context,
                                builder: (context) => GetTokenScreen(
                                    screenSize: widget.screenSize,
                                    newUserTokenController: textControllerToken,
                                    saveLocalTokenMethod:
                                        saveLocalTokenStorage),
                              );
                            },
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(30.0)),
                            child: Padding(
                              padding: EdgeInsets.only(
                                top: 15.0,
                                bottom: 15.0,
                              ),
                              child: Text(
                                Languages.of(context).labelGetToken,
                                style: TextStyle(
                                  fontSize: 14,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Visibility(
                    visible: isShortLinkAndCopyVisible,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Center(
                            child: InkWell(
                          splashColor: Colors.transparent,
                          hoverColor: Colors.transparent,
                          onTap: () {
                            URLLauncherHelper.launchInBrowser(
                                textControllerCurrentShortenUrl.text);
                          },
                          child: Text(
                            textControllerCurrentShortenUrl.text,
                            style: TextStyle(
                              color: Theme.of(context)
                                  .primaryTextTheme
                                  .button
                                  .color,
                              fontSize: 16,
                            ),
                          ),
                        )),
                        SizedBox(height: 10),
                        Center(
                            child: AnimatedIconButton(
                          size: 35,
                          onPressed: () {
                            FlutterClipboard.copy(
                                    textControllerCurrentShortenUrl.text)
                                .then((value) => print('copied: ' +
                                    textControllerCurrentShortenUrl.text));
                          },
                          duration: Duration(milliseconds: 200),
                          endIcon: Icon(
                            Icons.copy_outlined,
                            color: Colors.red,
                          ),
                          startIcon: Icon(
                            Icons.copy,
                            color: Colors.purple,
                          ),
                        ))
                      ],
                    ))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
