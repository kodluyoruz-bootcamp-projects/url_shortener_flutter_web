import 'dart:async';

import 'package:url_shortener_flutter_web/api/url_shortener_api/controllers/url_shortener_api_controller.dart';
import 'package:url_shortener_flutter_web/api/url_shortener_api/models/user.dart';
import 'package:url_shortener_flutter_web/localizations/languages/languages.dart';
import 'package:flutter/material.dart';
import 'package:url_shortener_flutter_web/utils/page_changer.dart';
import 'package:url_shortener_flutter_web/widgets/label_end_text_field.dart';
import 'package:url_shortener_flutter_web/widgets/loading.dart';
import 'package:url_shortener_flutter_web/widgets/loading_animation_button.dart';

import '../constant.dart';

class UserInfoScreen extends StatefulWidget {
  const UserInfoScreen({
    Key key,
    @required this.screenSize,
  }) : super(key: key);

  final Size screenSize;

  @override
  _UserInfoScreenState createState() => _UserInfoScreenState();
}

class _UserInfoScreenState extends State<UserInfoScreen> {
  TextEditingController textControllerToken;
  FocusNode textFocusNodeToken;
  bool _isEditingToken = false;

  TextEditingController textControllerEmail;
  bool _isUserInfoGet = false;
  FocusNode textFocusNodeEmail;
  bool _isEditingEmail = false;

  TextEditingController textControllerName;
  FocusNode textFocusNodeName;
  bool _isEditingName = false;

  bool nameVisibility = false;
  bool emailVisibility = false;
  bool _isGetUserInfoButtonPressed = false;

  String validateToken() {
    if (textControllerToken.text.trim().isEmpty) {
      return Languages.of(context).labelTokenNotValidMessage;
    }

    return null;
  }

  bool isValidateToken() {
    if (validateToken() == null) {
      return true;
    }
    return false;
  }

  String validateName() {
    if (textControllerName.text.trim().isEmpty) {
      return Languages.of(context).labelTokenNotValidMessage;
    }

    return null;
  }

  bool isValidateName() {
    if (validateName() == null) {
      return true;
    }
    return false;
  }

  String validateEmail() {
    String value = textControllerEmail.text.trim();

    if (value != null) {
      if (value.isEmpty) {
        return 'Email can\'t be empty';
      } else if (!value.contains(RegExp(
          r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+"))) {
        return 'Enter a correct email address';
      }
    }

    return null;
  }

  bool isValidateEmail() {
    if (validateEmail() == null) {
      return true;
    }
    return false;
  }

  void closeUserInfo() {
    setState(() {
      _isGetUserInfoButtonPressed = false;
    });
  }

  @override
  void initState() {
    textControllerToken = TextEditingController();
    textControllerToken.text = null;
    textFocusNodeToken = FocusNode();

    textControllerName = TextEditingController();
    textControllerName.text = null;
    textFocusNodeName = FocusNode();

    textControllerEmail = TextEditingController();
    textControllerEmail.text = null;
    textFocusNodeEmail = FocusNode();

    super.initState();
  }

  Future<bool> getUserInfo() async {
    setState(() {
      _isUserInfoGet = false;
      _isGetUserInfoButtonPressed = true;
    });
    String token = textControllerToken.text.trim();

    if (isValidateToken()) {
      User user = await URLShortenerAPIController.getUserInfo(token);

      if (user != null) {
        setState(() {
          print(user.name);
          textControllerName.text = user.name;
          print(user.email);
          textControllerEmail.text = user.email;
          _isUserInfoGet = true;
        });
        return true;
      }
    }

    setState(() {
      _isGetUserInfoButtonPressed = false;
    });

    return false;
  }

  Future<bool> updateUserInfo() async {
    String token = textControllerToken.text.trim();
    String name = textControllerName.text.trim();

    if (isValidateName()) {
      try {
        return await URLShortenerAPIController.updateUser(token, name);
      } catch (e) {
        return false;
      }
    }
    return false;
  }

  Future<bool> deleteUser() async {
    String token = textControllerToken.text.trim();
    if (isValidateName()) {
      try {
        return await URLShortenerAPIController.deleteUser(token);
      } catch (e) {
        return false;
      }
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: Theme.of(context).backgroundColor,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8.0),
      ),
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Container(
            width: 400,
            color: Theme.of(context).backgroundColor,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(height: 30),
                LabelAndTextField(
                    labelText: Languages.of(context).labelToken,
                    labelTextHint: Languages.of(context).labelToken,
                    textController: textControllerToken,
                    textFieldEnabled: true,
                    validateInput: validateToken),
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Flexible(
                        flex: 1,
                        child: Container(
                          width: double.maxFinite,
                          child: LoadingAnimationButton(
                              buttonText: Languages.of(context)
                                  .labelGetUserInfoButtonText,
                              color: kPrimaryButtonColor,
                              onPressedMethod: getUserInfo),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 10),
                Visibility(
                    visible: _isGetUserInfoButtonPressed,
                    child: _isUserInfoGet == false
                        ? Loading()
                        : Container(
                            child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                  left: 40.0,
                                  right: 40.0,
                                ),
                                child: Container(
                                  height: 1,
                                  width: double.maxFinite,
                                  color: Colors.blueGrey[200],
                                ),
                              ),
                              SizedBox(height: 30),
                              LabelAndTextField(
                                  labelText: Languages.of(context).labelName,
                                  labelTextHint:
                                      Languages.of(context).labelName,
                                  textController: textControllerName,
                                  textFieldEnabled: true,
                                  validateInput: validateName),
                              SizedBox(height: 30),
                              LabelAndTextField(
                                  labelText: Languages.of(context).labelEmail,
                                  labelTextHint:
                                      Languages.of(context).labelEmail,
                                  textController: textControllerEmail,
                                  textFieldEnabled: false,
                                  validateInput: validateEmail),
                              Padding(
                                padding: const EdgeInsets.all(20.0),
                                child: Row(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: [
                                    Flexible(
                                      flex: 1,
                                      child: Container(
                                        width: double.maxFinite,
                                        child: LoadingAnimationButton(
                                            buttonText:
                                                Languages.of(context).labelSave,
                                            color: kSecondaryButtonColor,
                                            onPressedMethod: updateUserInfo),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(0.0),
                                child: Row(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: [
                                    Flexible(
                                      flex: 1,
                                      child: Container(
                                        width: double.maxFinite,
                                        child: LoadingAnimationButton(
                                          buttonText: Languages.of(context)
                                              .labelDeleteAccount,
                                          color: kDangerZoneButtonColor,
                                          onPressedMethod: deleteUser,
                                          extraMethod: closeUserInfo,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          )))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
