import 'dart:async';

import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter/material.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:url_shortener_flutter_web/constant.dart';
import 'package:url_shortener_flutter_web/localizations/languages/languages.dart';
import 'package:url_shortener_flutter_web/screens/get_user_all_links_screen.dart';
import 'package:url_shortener_flutter_web/screens/get_user_links_statistics_screen.dart';
import 'package:url_shortener_flutter_web/screens/shorten_url_screen.dart';
import 'package:url_shortener_flutter_web/screens/user_info_screen.dart';
import 'package:url_shortener_flutter_web/utils/page_changer.dart';
import 'package:url_shortener_flutter_web/widgets/bottom_bar.dart';
import 'package:url_shortener_flutter_web/widgets/explore_drawer.dart';
import 'package:url_shortener_flutter_web/widgets/floating_quick_access_bar.dart';
import 'package:url_shortener_flutter_web/widgets/responsive.dart';
import 'package:url_shortener_flutter_web/widgets/top_bar_contents.dart';
import 'package:url_shortener_flutter_web/widgets/web_scrollbar.dart';

import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:dio/dio.dart';

class HomeScreen extends StatefulWidget {
  static const String route = '/';

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  ScrollController _scrollController;
  double _scrollPosition = 0;
  double _opacity = 0;
  final RoundedLoadingButtonController _btnController =
      new RoundedLoadingButtonController();

  void _doLoginButtonStartState() async {
    Timer(Duration(seconds: 1), () {
      //_btnController.error();
      //_btnController.success();
      //_btnController.stop();
      _btnController.stop();
    });
  }

  void _wait() {
    Timer(Duration(seconds: 3), () {
      //_btnController.error();
      //_btnController.success();
      //_btnController.stop();
      //_btnController.stop();
    });
  }

  void _doNavigateToHome() async {
    Timer(Duration(seconds: 1), () {
      /*Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => SideBarLayout()),
      );*/
    });
  }

  _scrollListener() {
    setState(() {
      _scrollPosition = _scrollController.position.pixels;
    });
  }

  @override
  void initState() {
    _scrollController = ScrollController();
    _scrollController.addListener(_scrollListener);
    PageChanger.changePage(OPENING_PAGE_INDEX);
    super.initState();
  }

  void stateSetter() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    _opacity = _scrollPosition < screenSize.height * 0.40
        ? _scrollPosition / (screenSize.height * 0.40)
        : 1;

    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      extendBodyBehindAppBar: true,
      appBar: ResponsiveWidget.isSmallScreen(context)
          ? AppBar(
              backgroundColor:
                  Theme.of(context).bottomAppBarColor.withOpacity(_opacity),
              elevation: 0,
              centerTitle: true,
              actions: [
                IconButton(
                  icon: Icon(Icons.brightness_6),
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  onPressed: () {
                    DynamicTheme.of(context).setBrightness(
                        Theme.of(context).brightness == Brightness.dark
                            ? Brightness.light
                            : Brightness.dark);
                    print(Theme.of(context).brightness);
                  },
                ),
              ],
              title: Text(
                Languages.of(context).appName,
                style: TextStyle(
                  color: Colors.blueGrey[100],
                  fontSize: 20,
                  fontFamily: 'Montserrat',
                  fontWeight: FontWeight.w400,
                  letterSpacing: 3,
                ),
              ),
            )
          : PreferredSize(
              preferredSize: Size(screenSize.width, 1000),
              child: TopBarContents(_opacity),
            ),
      drawer: ExploreDrawer(),
      body: WebScrollbar(
        color: Colors.blueGrey,
        backgroundColor: Colors.blueGrey.withOpacity(0.3),
        width: 10,
        heightFraction: 0.3,
        controller: _scrollController,
        child: SingleChildScrollView(
          controller: _scrollController,
          physics: ClampingScrollPhysics(),
          child: Column(
            children: [
              Stack(
                children: [
                  Container(
                    child: SizedBox(
                      height: screenSize.height * 0.45,
                      width: screenSize.width,
                      child: Image.asset(
                        'assets/images/cover.jpg',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Column(
                    children: [
                      FloatingQuickAccessBar(
                          screenSize: screenSize, stateSetter: stateSetter),
                      PageChanger.getCurrentPage() == PageChanger.PAGE_1_INDEX
                          ? ShortenUrlScreen(screenSize: screenSize)
                          : PageChanger.getCurrentPage() ==
                                  PageChanger.PAGE_2_INDEX
                              ? UserInfoScreen(screenSize: screenSize)
                              : PageChanger.getCurrentPage() ==
                                      PageChanger.PAGE_3_INDEX
                                  ? GetUserAllLinksScreen(
                                      screenSize: screenSize)
                                  : GetUserLinksStatisticsScreen(
                                      screenSize: screenSize),
                    ],
                  )
                ],
              ),
              // SizedBox(height: screenSize.height / 8),
              SizedBox(height: screenSize.height / 10),
              BottomBar(),
            ],
          ),
        ),
      ),
    );
  }
}
