import 'dart:async';

import 'package:animated_icon_button/animated_icon_button.dart';
import 'package:clipboard/clipboard.dart';
import 'package:flutter/services.dart';
import 'package:url_shortener_flutter_web/api/url_shortener_api/controllers/url_shortener_api_controller.dart';
import 'package:url_shortener_flutter_web/api/url_shortener_api/models/link.dart';
import 'package:url_shortener_flutter_web/screens/update_user_link_screen.dart';
import 'package:url_shortener_flutter_web/localizations/languages/languages.dart';
import 'package:flutter/material.dart';
import 'package:url_shortener_flutter_web/utils/url_launcher_helper.dart';
import 'package:url_shortener_flutter_web/widgets/label_end_text_field.dart';
import 'package:url_shortener_flutter_web/widgets/loading.dart';
import 'package:url_shortener_flutter_web/widgets/loading_animation_button.dart';

import '../constant.dart';

class GetUserAllLinksScreen extends StatefulWidget {
  const GetUserAllLinksScreen({
    Key key,
    @required this.screenSize,
  }) : super(key: key);

  final Size screenSize;

  @override
  _GetUserAllLinksScreenState createState() => _GetUserAllLinksScreenState();
}

class _GetUserAllLinksScreenState extends State<GetUserAllLinksScreen> {
  List<Link> links;

  TextEditingController textControllerToken;
  FocusNode textFocusNodeToken;
  bool _isEditingToken = false;

  TextEditingController textControllerEmail;
  bool _isUserInfoGet = false;
  FocusNode textFocusNodeEmail;
  bool _isEditingEmail = false;

  TextEditingController textControllerName;
  FocusNode textFocusNodeName;
  bool _isEditingName = false;

  bool nameVisibility = false;
  bool emailVisibility = false;
  bool _isGetUserInfoButtonPressed = false;

  String validateToken() {
    if (textControllerToken.text.trim().isEmpty) {
      return Languages.of(context).labelTokenNotValidMessage;
    }

    return null;
  }

  bool isValidateToken() {
    if (validateToken() == null) {
      return true;
    }
    return false;
  }

  String validateName() {
    if (textControllerName.text.trim().isEmpty) {
      return Languages.of(context).labelTokenNotValidMessage;
    }

    return null;
  }

  bool isValidateName() {
    if (validateName() == null) {
      return true;
    }
    return false;
  }

  String validateEmail() {
    String value = textControllerEmail.text.trim();

    if (value != null) {
      if (value.isEmpty) {
        return 'Email can\'t be empty';
      } else if (!value.contains(RegExp(
          r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+"))) {
        return 'Enter a correct email address';
      }
    }

    return null;
  }

  bool isValidateEmail() {
    if (validateEmail() == null) {
      return true;
    }
    return false;
  }

  Future<bool> deleteLink(int index) async {
    String token = textControllerToken.text.trim();

    try {
      return await URLShortenerAPIController.deleteLink(token, links[index].id);
    } catch (e) {
      return false;
    }
  }

  void refreshLinks() {
    setState(() {
      _isGetUserInfoButtonPressed = false;
    });
  }

  @override
  void initState() {
    textControllerToken = TextEditingController();
    textControllerToken.text = null;
    textFocusNodeToken = FocusNode();

    textControllerName = TextEditingController();
    textControllerName.text = null;
    textFocusNodeName = FocusNode();

    textControllerEmail = TextEditingController();
    textControllerEmail.text = null;
    textFocusNodeEmail = FocusNode();

    super.initState();
  }

  Future<bool> getUserAllLinks() async {
    setState(() {
      _isUserInfoGet = false;
      _isGetUserInfoButtonPressed = true;
    });
    String token = textControllerToken.text.trim();

    if (isValidateToken()) {
      links = await URLShortenerAPIController.getUserAllLinks(token, context);

      if (links != null) {
        setState(() {
          _isUserInfoGet = true;
        });
        return true;
      }
    }

    setState(() {
      _isGetUserInfoButtonPressed = false;
    });

    return false;
  }

  Future<void> openUpdateUserLinksDialog(int index) async {
    showDialog(
      context: context,
      builder: (context) => new UpdateUserLinkScreen(
        screenSize: widget.screenSize,
        token: textControllerToken.text,
        linkId: links[index].id,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: Theme.of(context).backgroundColor,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8.0),
      ),
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Container(
            width: 400,
            color: Theme.of(context).backgroundColor,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(height: 30),
                LabelAndTextField(
                    labelText: Languages.of(context).labelToken,
                    labelTextHint: Languages.of(context).labelToken,
                    textController: textControllerToken,
                    textFieldEnabled: true,
                    validateInput: validateToken),
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Flexible(
                        flex: 1,
                        child: Container(
                          width: double.maxFinite,
                          child: LoadingAnimationButton(
                              buttonText: Languages.of(context)
                                  .labelGetUserAllLinksButtonText,
                              color: kPrimaryButtonColor,
                              onPressedMethod: getUserAllLinks),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 10),
                Visibility(
                    visible: _isGetUserInfoButtonPressed,
                    child: _isUserInfoGet == false
                        ? Loading()
                        : Container(
                            child: ListView.builder(
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            padding: const EdgeInsets.all(10.0),
                            itemCount: links.length,
                            itemBuilder: (context, i) {
                              return _buildRow(links[i], i);
                            },
                          )))
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildRow(Link link, int index) {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Flexible(
            flex: 3,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Flexible(
                  child: new Container(
                    padding: new EdgeInsets.only(right: 13.0),
                    child: InkWell(
                      splashColor: Colors.transparent,
                      hoverColor: Colors.transparent,
                      onTap: () {
                        URLLauncherHelper.launchInBrowser(link.shortLink);
                      },
                      child: Text(
                        link.shortLink,
                        style: TextStyle(
                          color:
                              Theme.of(context).primaryTextTheme.button.color,
                          fontSize: 16,
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 5),
                Flexible(
                  child: new Container(
                    padding: new EdgeInsets.only(right: 13.0),
                    child: new Text(
                      link.targetLink,
                      overflow: TextOverflow.ellipsis,
                      style: new TextStyle(
                        fontSize: 13.0,
                        fontFamily: 'Roboto',
                        color: Colors.amberAccent.shade700,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 5),
                Flexible(
                  child: new Container(
                      padding: new EdgeInsets.only(right: 13.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Text(
                            Languages.of(context).labelStatus + " : ",
                            overflow: TextOverflow.ellipsis,
                            style: new TextStyle(
                              fontSize: 13.0,
                              fontFamily: 'Roboto',
                              color: Colors.blue,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Text(
                            link.status,
                            overflow: TextOverflow.ellipsis,
                            style: new TextStyle(
                              fontSize: 13.0,
                              fontFamily: 'Roboto',
                              color: link.color,
                              fontWeight: FontWeight.bold,
                            ),
                          )
                        ],
                      )),
                ),
                SizedBox(height: 5),
                Flexible(
                  child: new Container(
                      padding: new EdgeInsets.only(right: 13.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Text(
                            Languages.of(context).labelUsageCounter + " : ",
                            overflow: TextOverflow.ellipsis,
                            style: new TextStyle(
                              fontSize: 13.0,
                              fontFamily: 'Roboto',
                              color: Colors.blue,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Text(
                            link.usageCounter.toString(),
                            overflow: TextOverflow.ellipsis,
                            style: new TextStyle(
                              fontSize: 13.0,
                              fontFamily: 'Roboto',
                              color: link.color,
                              fontWeight: FontWeight.bold,
                            ),
                          )
                        ],
                      )),
                )
              ],
            ),
          ),
          Flexible(
            flex: 1,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Center(
                    child: AnimatedIconButton(
                  size: 35,
                  onPressed: () {
                    FlutterClipboard.copy(link.shortLink)
                        .then((value) => print('copied: ' + link.shortLink));
                  },
                  duration: Duration(milliseconds: 200),
                  endIcon: Icon(
                    Icons.copy_outlined,
                    color: Colors.red,
                  ),
                  startIcon: Icon(
                    Icons.copy,
                    color: Colors.purple,
                  ),
                )),
                SizedBox(height: 5),
                Container(
                  width: double.maxFinite,
                  child: FlatButton(
                    color: kSecondaryButtonColor,
                    hoverColor: kSecondaryButtonColor,
                    highlightColor: Colors.black,
                    onPressed: () async {
                      openUpdateUserLinksDialog(index);
                    },
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(30.0)),
                    child: Text(
                      Languages.of(context).labelEdit,
                      style: TextStyle(
                        fontSize: 11,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 10),
                Container(
                  width: double.maxFinite,
                  child: LoadingAnimationButton(
                    buttonText: Languages.of(context).labelDelete,
                    color: kDangerZoneButtonColor,
                    onPressedMethod: () => deleteLink(index),
                    extraMethod: getUserAllLinks,
                  ),
                ),
                SizedBox(height: 5),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
