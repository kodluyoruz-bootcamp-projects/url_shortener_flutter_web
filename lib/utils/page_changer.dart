import 'package:flutter/material.dart';

class PageChanger {
  static int currentPageIndex = 0;

  static const int PAGE_1_INDEX = 0;
  static const int PAGE_2_INDEX = 1;
  static const int PAGE_3_INDEX = 2;
  static const int PAGE_4_INDEX = 3;

  static void changePage(int newPageIndex) {
    currentPageIndex = newPageIndex;
  }

  static int getCurrentPage() {
    return currentPageIndex;
  }
}
